Description: This mod gives two new item called Squeezed iron and Squeezed copper. Makes iron and copper smelting easier.
Depencies: default (Optional)
License for code: LGPL 2.1
License for textures: CC--BY-SA 4.0
original iron and copper texture comes from the default
Version: 0.2
