--Registering the squeezed iron
minetest.register_craftitem("ironsqueezer:squeezed_iron", {
    description = "Squeezed Iron",
    inventory_image = "IronSqueezer_Squeezed_Iron.png"
})

--Crafting the squeezed iron
minetest.register_craft({
    output = "ironsqueezer:squeezed_iron",
    recipe = {
        {"default:iron_lump", "default:iron_lump"},
        {"default:iron_lump", "default:iron_lump"}
    }
})

--Smelting the squeezed iron into steel ingots
minetest.register_craft({
    type = "cooking",
    output = "default:steel_ingot 4",
    recipe = "ironsqueezer:squeezed_iron",
    cooktime = 11,
})

--Registering the squeezed copper
minetest.register_craftitem("ironsqueezer:squeezed_copper", {
    description = "Squeezed Copper",
    inventory_image = "IronSqueezer_Squeezed_Copper.png"
})

--Crafting the squeezed copper
minetest.register_craft({
    output = "ironsqueezer:squeezed_copper",
    recipe = {
        {"default:copper_lump", "default:copper_lump"},
        {"default:copper_lump", "default:copper_lump"}
    }
})

--Smelting the squeezed copper into copper ingots
minetest.register_craft({
    type = "cooking",
    output = "default:copper_ingot 4",
    recipe = "ironsqueezer:squeezed_copper",
    cooktime = 11,
})
