# iron squeezer

A mod made for MineTest that gives two items named *squeezed iron* and *squeezed copper*, you can use them to make some space in your inventory while mining.
They also smelt a little bit faster than the normal lumps.